import React, { Component } from 'react'
import PropTypes from 'prop-types'

import cardFace from 'assets/svgs/ambercard_face.svg'
import cardBack from 'assets/svgs/ambercard_back.svg'

import './DiscountCard.css'

export default class DiscountCard extends Component {
  static propTypes = {
    showCard: PropTypes.bool,
    toggleCard: PropTypes.func,
  }

  render() {
    return (
      <div onClick={this.props.toggleCard} className={`top-card${this.props.showCard ? ' flipped' : ''}`}>
        <div className="top-card__face" style={{ backgroundImage: `url(${cardFace})` }} />
        <div className="top-card__back" style={{ backgroundImage: `url(${cardBack})` }} />
      </div>
    )
  }
}
