import React, { Component } from 'react'
import PropTypes from 'prop-types'

import logoGray from 'assets/svgs/logoBlack.svg'

import './FixedLine.css'

export default class FixedLine extends Component {
  static propTypes = {
    toggleFilter: PropTypes.func,
  }

  render() {
    return (
      <div className="outgoing-line line-styles">
        <img src={logoGray} alt="Logo" />
        <span className="outgoing-line__show" onClick={this.props.toggleFilter}>
          Фильтровать места
        </span>
      </div>
    )
  }
}
