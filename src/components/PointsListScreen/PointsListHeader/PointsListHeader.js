import React, { Component } from 'react'
import { CSSTransition } from 'react-transition-group'
import PropTypes from 'prop-types'

import DiscountCard from './DiscountCard'
import FixedLine from './FixedLine'

import icCircle from 'assets/svgs/icCircle.svg'

import './PointsListHeader.css'

export default class PointsListHeader extends Component {
  static propTypes = {
    toggleFilter: PropTypes.func,
  }

  state = {
    showTopLine: false,
    showCard: false,
  }

  componentDidMount() {
    window.addEventListener('scroll', this.toggleLine)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.toggleLine)
  }

  toggleLine = () => {
    if (window.pageYOffset >= 20) {
      this.setState({
        showTopLine: true,
      })
    } else {
      this.setState({
        showTopLine: false,
      })
    }
  }

  toggleCard = () => {
    this.setState({
      showCard: !this.state.showCard,
    })
  }

  render() {
    return (
      <div className="top-list line-styles">
        <CSSTransition in={this.state.showTopLine} timeout={300} classNames="showed">
          <FixedLine toggleFilter={this.props.toggleFilter} />
        </CSSTransition>
        <DiscountCard showCard={this.state.showCard} toggleCard={this.toggleCard} />
        <div className="top-decorate">
          <img src={icCircle} alt="circle" />
        </div>
      </div>
    )
  }
}
