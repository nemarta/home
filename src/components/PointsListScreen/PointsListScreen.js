import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { CSSTransition } from 'react-transition-group'

import Loader from 'components/common/Loader'
import PointsListHeader from './PointsListHeader'
import PointListItem from './PointListItem'
import Filter from './Filter'
import Search from './Search'
import NotFound from 'components/common/NotFound'

import './PointsListScreen.css'

export default class PointsListScreen extends Component {
  static propTypes = {
    points: PropTypes.array,
    isFetching: PropTypes.bool,
    checkFilter: PropTypes.bool,
    contentThunk: PropTypes.func,
    locationThunk: PropTypes.func,
    clearSelectedCategory: PropTypes.func,
    onlyFavoriteClear: PropTypes.func,
  }

  state = {
    visibleFilter: false,
  }

  componentDidMount() {
    const { contentThunk, locationThunk } = this.props
    contentThunk()
    locationThunk()
  }

  toggleFilter = () => {
    this.setState({
      visibleFilter: !this.state.visibleFilter,
    })
  }

  clearAll = () => {
    let { clearSelectedCategory, onlyFavoriteClear } = this.props
    onlyFavoriteClear()
    clearSelectedCategory()
  }

  render() {
    const { points, isFetching, checkFilter } = this.props
    let { visibleFilter } = this.state
    if (isFetching) {
      return (
        <div className="points-list">
          <Loader />
        </div>
      )
    } else if (points !== null) {
      return (
        <div className="points-list">
          <PointsListHeader toggleFilter={this.toggleFilter} />
          {ReactDOM.createPortal(
            <CSSTransition in={visibleFilter} timeout={300} classNames="showed">
              <Filter toggleFilter={this.toggleFilter} />
            </CSSTransition>,
            document.getElementById('modal'),
          )}
          <Search />
          <ul className="list-items">
            {points.length > 0 ? (
              points.map((point, id) => <PointListItem key={id} point={point} />)
            ) : (
              <NotFound clearAll={this.clearAll} showClearFilter={false} />
            )}
          </ul>
          {checkFilter ? (
            <button className="not-found__button" onClick={this.clearAll}>
              Очистить фильтр
            </button>
          ) : null}
        </div>
      )
    } else {
      return (
        <div className="points-list">
          <PointsListHeader toggleFilter={this.toggleFilter} />
          {ReactDOM.createPortal(
            <CSSTransition in={visibleFilter} timeout={300} classNames="showed">
              <Filter toggleFilter={this.toggleFilter} />
            </CSSTransition>,
            document.getElementById('modal'),
          )}
          <NotFound clearAll={this.clearAll} showClearFilter={true} />
        </div>
      )
    }
  }
}
