import { connect } from 'react-redux'
import PointsListScreen from './PointsListScreen'
import { getIsFetching, getPointsSortedAndFiltered } from 'reducers/content/selectors'
import { contentThunk } from 'reducers/content/actions'
import { locationThunk } from 'reducers/location/actions'
import { clearSelectedCategory, onlyFavoriteClear } from 'reducers/filter/actions'
import { onlyFavorite, getFilteredCategory } from 'reducers/filter/selectors'

const mapStateToProps = state => ({
  isFetching: getIsFetching(state),
  points: getPointsSortedAndFiltered(state),
  checkFilter: onlyFavorite(state) || getFilteredCategory(state) ? true : false,
})

export default connect(mapStateToProps, {
  contentThunk,
  locationThunk,
  clearSelectedCategory,
  onlyFavoriteClear,
})(PointsListScreen)
