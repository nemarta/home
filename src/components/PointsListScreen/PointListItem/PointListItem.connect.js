import PointListItem from './PointListItem'
import { connect } from 'react-redux'
import { getFullCategoriesByIds } from 'reducers/content/selectors'

const mapStateToProps = (state, ownProps) => ({
  categories: getFullCategoriesByIds(state, ownProps.point.category_id),
})

export default connect(mapStateToProps)(PointListItem)
