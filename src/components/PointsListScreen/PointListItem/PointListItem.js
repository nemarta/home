import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import PointHeader from 'components/common/PointHeader'
import PointName from 'components/common/PointName'
import PointParagraph from 'components/common/PointParagraph'

import DiscountFlag from './DiscountFlag'
import Distance from './Distance'

import './PointListItem.css'

export default class PointListItem extends Component {
  static propTypes = {
    point: PropTypes.object,
    categories: PropTypes.array,
  }

  render() {
    const { point, categories } = this.props
    let { id: idPoint, discount_max: discount, name, description } = point
    return (
      <li className="list-item">
        <PointHeader categories={categories}>
          <DiscountFlag discount={discount} />
        </PointHeader>
        <Link to={{ pathname: `/point/${idPoint}` }}>
          <PointName name={name} />
        </Link>
        <PointParagraph description={description} limitCharacters={100} />
        <div className="item-footer">
          <Distance idPoint={idPoint} />
        </div>
      </li>
    )
  }
}
