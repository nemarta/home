import Distance from './Distance'
import { connect } from 'react-redux'
import { getDistance } from 'reducers/content/selectors'

const mapStateToProps = (state, ownProps) => ({
  distance: getDistance(state, ownProps.idPoint),
})

export default connect(mapStateToProps)(Distance)
