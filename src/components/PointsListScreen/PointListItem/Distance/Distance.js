import React, { Component } from 'react'
import PropTypes from 'prop-types'

import locationImage from 'assets/svgs/location.svg'

import './Distance.css'

export default class Distance extends Component {
  static propTypes = {
    distance: PropTypes.object,
  }

  render() {
    const { distance } = this.props
    if (distance) {
      return (
        <div className="distance">
          <img className="distance__image" src={locationImage} alt="Distance" />
          <span className="distance__value">
            {distance.convertedData.value} {distance.convertedData.unit === 'meters' ? 'м' : 'км'}
          </span>
        </div>
      )
    }
    return null
  }
}
