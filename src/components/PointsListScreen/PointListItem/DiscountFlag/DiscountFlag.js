import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './DiscountFlag.css'

export default class DiscountFlag extends Component {
  static propTypes = {
    discount: PropTypes.string,
  }

  render() {
    const { discount } = this.props
    if (discount !== '0') {
      return <span className="item-header__sale">-{this.props.discount}%</span>
    } else {
      return null
    }
  }
}
