import React, { Component } from 'react'
import PropTypes from 'prop-types'

import FilterLine from './FilterLine'
import ClearFilter from './ClearFilter'

import closeIcon from 'assets/svgs/close.svg'
import heartEmpty from 'assets/svgs/heart_empty.svg'
import heartFull from 'assets/svgs/heart_full.svg'

import './Filter.css'

export default class Filter extends Component {
  static propTypes = {
    categories: PropTypes.array,
    onlyFavorite: PropTypes.bool,
    selectedCategory: PropTypes.number,
    selectCategory: PropTypes.func,
    toggleFilter: PropTypes.func,
    clearSelectedCategory: PropTypes.func,
    toggleOnlyFavorite: PropTypes.func,
    onlyFavoriteClear: PropTypes.func,
  }

  filterOnlyFavorite = () => {
    const { toggleOnlyFavorite, clearSelectedCategory, toggleFilter } = this.props
    toggleOnlyFavorite()
    clearSelectedCategory()
    toggleFilter()
  }

  render() {
    const {
      categories,
      onlyFavorite,
      selectedCategory,
      selectCategory,
      toggleFilter,
      clearSelectedCategory,
      toggleOnlyFavorite,
      onlyFavoriteClear,
    } = this.props
    if (categories) {
      return (
        <div className="filter">
          <div className="filter-header">
            <img onClick={toggleFilter} className="filter-header__icon" src={closeIcon} alt="close" />
            Фильтровать места
          </div>
          <div className="filter-body">
            {categories.map((category, id) => (
              <FilterLine
                active={selectedCategory === category.id}
                key={id}
                category={category}
                selectCategory={selectCategory}
                toggleFilter={toggleFilter}
                onlyFavorite={onlyFavorite}
                toggleOnlyFavorite={toggleOnlyFavorite}
              />
            ))}
            <div onClick={this.filterOnlyFavorite} className={`filter-body__line${onlyFavorite ? ' active' : ''}`}>
              <img className="filter-body__icon" src={onlyFavorite ? heartFull : heartEmpty} alt="icon" />
              Только избранное
            </div>
            <ClearFilter
              selectCategory={selectCategory}
              toggleFilter={toggleFilter}
              clearSelectedCategory={clearSelectedCategory}
              onlyFavoriteClear={onlyFavoriteClear}
            />
          </div>
        </div>
      )
    } else {
      return null
    }
  }
}
