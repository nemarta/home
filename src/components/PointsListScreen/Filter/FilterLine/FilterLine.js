import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './FilterLine.css'

export default class FilterLine extends Component {
  static propTypes = {
    category: PropTypes.object,
    active: PropTypes.bool,
    onlyFavorite: PropTypes.bool,
    selectCategory: PropTypes.func,
    toggleFilter: PropTypes.func,
    toggleOnlyFavorite: PropTypes.func,
  }

  selectCategory = () => {
    const { category, onlyFavorite, selectCategory, toggleFilter, toggleOnlyFavorite } = this.props
    toggleFilter()
    if (onlyFavorite) toggleOnlyFavorite()
    selectCategory(category.id)
  }

  render() {
    const { category, active } = this.props
    return (
      <div onClick={this.selectCategory} className={active ? 'filter-body__line active' : 'filter-body__line'}>
        <img className="filter-body__icon" src={category.icon} alt="icon" />
        {category.name}
      </div>
    )
  }
}
