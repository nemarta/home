import { connect } from 'react-redux'
import Filter from './Filter'
import { getCategories } from 'reducers/content/selectors'
import { getFilteredCategory, onlyFavorite } from 'reducers/filter/selectors'
import { selectCategory, clearSelectedCategory, toggleOnlyFavorite, onlyFavoriteClear } from 'reducers/filter/actions'

const mapStateToProps = state => ({
  categories: getCategories(state),
  onlyFavorite: onlyFavorite(state),
  selectedCategory: getFilteredCategory(state),
})

export default connect(mapStateToProps, {
  selectCategory,
  clearSelectedCategory,
  toggleOnlyFavorite,
  onlyFavoriteClear,
})(Filter)
