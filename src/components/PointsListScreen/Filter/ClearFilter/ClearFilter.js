import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './ClearFilter.css'

export default class ClearFilter extends Component {
  static propTypes = {
    toggleFilter: PropTypes.func,
    clearSelectedCategory: PropTypes.func,
    onlyFavoriteClear: PropTypes.func,
  }

  selectCategory = () => {
    const { toggleFilter, clearSelectedCategory, onlyFavoriteClear } = this.props
    toggleFilter()
    clearSelectedCategory()
    onlyFavoriteClear()
  }

  render() {
    return (
      <button onClick={this.selectCategory} className="filter-body__button">
        Очистить фильтр
      </button>
    )
  }
}
