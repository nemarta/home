import Search from './Search'
import { connect } from 'react-redux'
import { setKeywords, clearKeywords } from 'reducers/filter/actions'
import { getKeywords } from 'reducers/filter/selectors'

const mapStateToProps = state => ({
  keywords: getKeywords(state),
})

export default connect(mapStateToProps, {
  setKeywords,
  clearKeywords,
})(Search)
