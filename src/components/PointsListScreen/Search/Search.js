import React, { Component } from 'react'
import PropTypes from 'prop-types'

import search from 'assets/svgs/magnifying-glass.svg'
import clear from 'assets/svgs/close_gray.svg'

import './Search.css'

export default class Search extends Component {
  static propTypes = {
    keywords: PropTypes.string,
    setKeywords: PropTypes.func,
    clearKeywords: PropTypes.func,
  }

  state = {
    isSearchVisible: this.props.keywords !== '',
  }

  toggleSearch = () => {
    this.setState({
      isSearchVisible: !this.state.isSearchVisible,
    })
  }

  inputQuery = event => {
    const { setKeywords } = this.props
    let val = event.target.value
    setKeywords(val)
    if (val.length === 0) this.toggleSearch()
  }

  clearQuery = () => {
    const { clearKeywords } = this.props
    clearKeywords()
    this.toggleSearch()
  }

  render() {
    return (
      <div className="search-container">
        <div onClick={this.toggleSearch} className={`search-button${this.state.isSearchVisible ? ' hidden' : ''}`}>
          <img src={search} alt="search" />
        </div>
        <div className={`search-input${this.state.isSearchVisible ? '' : ' hidden'}`}>
          <input
            value={this.props.keywords}
            onChange={this.inputQuery}
            placeholder="Введите ключевые слова"
            type="text"
            className="default-input search-input__input"
          />
          <div onClick={this.clearQuery} className="search-input__clear">
            <img src={clear} alt="clear" />
          </div>
        </div>
      </div>
    )
  }
}
