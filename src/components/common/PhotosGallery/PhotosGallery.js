import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class PhotosGallery extends Component {
  static propTypes = {
    photos: PropTypes.array,
  }

  render() {
    return this.props.photos.map((src, id) => <img key={id} className="point-photo" src={src} alt="point" />)
  }
}
