import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './PointName.css'

export default class PointName extends Component {
  static propTypes = {
    name: PropTypes.string,
    size: PropTypes.number,
  }

  render() {
    const { name, size } = this.props
    return (
      <h2 className="item-content__title" style={{ fontSize: `${size}px` }}>
        {name}
      </h2>
    )
  }
}
