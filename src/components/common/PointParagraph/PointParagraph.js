import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './PointParagraph.css'

import { cutTextByLimitCharacters } from 'helpers'

export default class PointParagraph extends Component {
  static propTypes = {
    description: PropTypes.string,
    limitCharacters: PropTypes.number,
    caption: PropTypes.string,
  }

  componentWillMount() {
    const { limitCharacters, description } = this.props
    if (limitCharacters && description) {
      let newDescription = cutTextByLimitCharacters(description, limitCharacters)
      this.setState({
        description: newDescription,
      })
    } else if (description) {
      this.setState({
        description: description,
      })
    } else {
      return null
    }
  }

  render() {
    const { caption, description } = this.props
    if (description) {
      return (
        <p className="item-content__description">
          {caption ? <b>{caption}: </b> : false}
          {this.state.description}
        </p>
      )
    } else {
      return null
    }
  }
}
