import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './NotFound.css'

export default class NotFound extends Component {
  static propTypes = {
    NotFound: PropTypes.bool,
    clearAll: PropTypes.func,
  }

  render() {
    return (
      <div className="not-found list-items">
        Ничего не найдено
        {this.props.NotFound ? (
          <button className="not-found__button" onClick={this.props.clearAll}>
            Очистить фильтр
          </button>
        ) : null}
      </div>
    )
  }
}
