import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class PointCategory extends Component {
  static propTypes = {
    icon: PropTypes.string,
    name: PropTypes.string,
  }

  render() {
    const { icon, name } = this.props
    return (
      <div className="item-header__category">
        <img className="item-header__image" src={icon} alt="Category" />
        <span className="item-header__title">{name}</span>
      </div>
    )
  }
}
