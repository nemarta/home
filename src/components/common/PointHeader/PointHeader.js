import React, { Component } from 'react'
import PropTypes from 'prop-types'

import PointCategory from './PointCategory'

import './PointHeader.css'

export default class PointHeader extends Component {
  static propTypes = {
    categories: PropTypes.array,
    children: PropTypes.element,
  }

  render() {
    const { categories, children } = this.props
    return (
      <div className="item-header">
        <div className="item-header__categories">
          {categories
            ? categories.map((category, id) => <PointCategory key={id} icon={category.icon} name={category.name} />)
            : null}
        </div>
        {children}
      </div>
    )
  }
}
