import React, { Component } from 'react'
import Slider from 'react-slick'
import PropTypes from 'prop-types'

import close from 'assets/svgs/close_white.svg'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import './GalleryScreen.css'

export default class GalleryScreen extends Component {
  static propTypes = {
    settings: PropTypes.object,
    photos: PropTypes.array,
    toggleGallery: PropTypes.func,
  }

  render() {
    return (
      <div className="photo-gallery-wrapper">
        <div className="photo-gallery">
          <div className="photo-gallery__close" onClick={this.props.toggleGallery}>
            <img src={close} alt="close" />
          </div>
          <Slider {...this.props.settings}>
            {this.props.photos.map((src, id) => (
              <img
                onClick={this.toggleGallery}
                key={id}
                className="point-photo point-photo--full-gallery"
                src={src}
                alt="point"
              />
            ))}
          </Slider>
        </div>
      </div>
    )
  }
}
