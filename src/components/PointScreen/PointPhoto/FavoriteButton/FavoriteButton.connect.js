import FavoriteButton from './FavoriteButton'
import { connect } from 'react-redux'
import { isFavorite } from 'reducers/favorite/selectors'
import { favoriteAdd, favoriteRemove } from 'reducers/favorite/actions'

const mapStateToProps = (state, ownProps) => ({
  isFavorite: isFavorite(state, ownProps.pointId),
})

export default connect(mapStateToProps, {
  favoriteAdd,
  favoriteRemove,
})(FavoriteButton)
