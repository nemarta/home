import React, { Component } from 'react'
import PropTypes from 'prop-types'

import fullHeart from 'assets/svgs/heart_full.svg'
import emptyHeart from 'assets/svgs/heart_empty.svg'

import './FavoriteButton.css'

export default class FavoriteButton extends Component {
  static propTypes = {
    pointId: PropTypes.number,
    isFavorite: PropTypes.bool,
    favoriteAdd: PropTypes.func,
    favoriteRemove: PropTypes.func,
  }

  clickHandler = () => {
    const { pointId, isFavorite, favoriteAdd, favoriteRemove } = this.props
    !isFavorite ? favoriteAdd(pointId) : favoriteRemove(pointId)
  }

  render() {
    return (
      <div onClick={this.clickHandler} className="favorite-button">
        <img src={this.props.isFavorite ? fullHeart : emptyHeart} alt="Heart" />
      </div>
    )
  }
}
