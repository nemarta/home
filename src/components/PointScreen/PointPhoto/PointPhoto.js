import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Slider from 'react-slick'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { CSSTransition } from 'react-transition-group'

import FavoriteButton from './FavoriteButton'
import Gallery from './GalleryScreen'

import backButton from 'assets/svgs/back.svg'

import './PointPhoto.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

export default class PointPhoto extends Component {
  static propTypes = {
    photos: PropTypes.array,
    pointId: PropTypes.number,
  }

  state = {
    galleryVisible: false,
  }

  toggleGallery = () => {
    this.setState({
      galleryVisible: !this.state.galleryVisible,
    })
  }

  render() {
    const { photos, pointId } = this.props
    const settings = {
      dots: false,
      arrows: false,
      infinite: false,
    }
    return (
      <div className="point-photo-wrapper">
        <Slider {...settings}>
          {photos.map((src, id) => (
            <img onClick={this.toggleGallery} key={id} className="point-photo" src={src} alt="point" />
          ))}
        </Slider>
        <Link className="back-button" to="/points">
          <img src={backButton} className="back-button__icon" alt="back" />
        </Link>
        <FavoriteButton pointId={pointId} />
        {ReactDOM.createPortal(
          <CSSTransition in={this.state.galleryVisible} timeout={300} classNames="showed">
            <Gallery toggleGallery={this.toggleGallery} photos={photos} settings={settings} />
          </CSSTransition>,
          document.getElementById('modal'),
        )}
      </div>
    )
  }
}
