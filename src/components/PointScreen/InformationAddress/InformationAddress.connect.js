import InformationAddress from './InformationAddress'
import { connect } from 'react-redux'
import { getPointAddress, getIsFetching } from 'reducers/address/selectors'

const mapStateToProps = (state, ownProps) => {
  return {
    isFetching: getIsFetching(state),
    address: getPointAddress(state, ownProps.addressId),
  }
}

export default connect(mapStateToProps)(InformationAddress)
