import React, { Component } from 'react'
import PropTypes from 'prop-types'

import Location from 'assets/svgs/location.svg'

import './InformationAddress.css'

export default class InformationAddress extends Component {
  static propTypes = {
    address: PropTypes.object,
    isFetching: PropTypes.bool,
  }

  render() {
    const { address, isFetching } = this.props
    if (address) {
      return (
        <div className="info-line">
          <img className="info-line__icon" src={Location} alt="Icon" />
          {isFetching ? (
            <div className="mini-loader" />
          ) : (
            <span className="info-line__value">{address.payload ? address.payload : '-'}</span>
          )}
        </div>
      )
    }
    return null
  }
}
