import { connect } from 'react-redux'
import PointScreen from './PointScreen'
import { getSelectedPoint, getIsFetching } from 'reducers/content/selectors'
import { addressThunk } from 'reducers/address/actions'
import { locationThunk } from 'reducers/location/actions'
import { contentThunk } from 'reducers/content/actions'
import { getMainLocation } from 'reducers/location/selectors'
import { getFullCategoriesByIds } from 'reducers/content/selectors'
import { getError } from 'reducers/location/selectors'

const mapStateToProps = (state, ownProps) => {
  let pointId = ownProps.match.params.id
  let selectedPoint = getSelectedPoint(state, pointId)
  return {
    isFetching: getIsFetching(state),
    point: selectedPoint,
    categories: selectedPoint ? getFullCategoriesByIds(state, selectedPoint.category_id) : null,
    mainLocation: getMainLocation(state),
    locationError: getError(state),
  }
}

export default connect(mapStateToProps, {
  addressThunk,
  contentThunk,
  locationThunk,
})(PointScreen)
