import React, { Component } from 'react'
import PropTypes from 'prop-types'

import emptyStar from 'assets/svgs/star_empty.svg'
import fullStar from 'assets/svgs/star_full.svg'

export default class PointRating extends Component {
  static propTypes = {
    rate: PropTypes.string,
  }

  render() {
    let ratingArray = [1, 2, 3, 4, 5]
    const { rate } = this.props
    return (
      <div className="rating">
        {ratingArray.map((element, id) => <img key={id} src={+rate < element ? emptyStar : fullStar} alt="star" />)}
      </div>
    )
  }
}
