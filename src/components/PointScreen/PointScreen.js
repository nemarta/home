import React, { Component } from 'react'

import PropTypes from 'prop-types'

import PointHeader from 'components/common/PointHeader'
import PointName from 'components/common/PointName'
import PointParagraph from 'components/common/PointParagraph'

import PointPhoto from './PointPhoto'
import PointRating from './PointRating'
import InformationAddress from './InformationAddress'
import Map from './Map'

import Phone from 'assets/svgs/phone.svg'
import Link from 'assets/svgs/link-symbol.svg'

import './PointScreen.css'

export default class PointScreen extends Component {
  static propTypes = {
    point: PropTypes.object,
    mainLocation: PropTypes.object,
    locationError: PropTypes.string,
    categories: PropTypes.array,
    match: PropTypes.object,
    addressThunk: PropTypes.func,
    contentThunk: PropTypes.func,
    locationThunk: PropTypes.func,
  }

  componentDidMount() {
    const { addressThunk, contentThunk, locationThunk, point, match, locationError, mainLocation } = this.props
    if (locationError || !mainLocation) locationThunk()
    if (point) {
      addressThunk(point.id, point.latitude, point.longitude)
    } else {
      contentThunk(match.params.id).then(response => {
        addressThunk(response.id, response.latitude, response.longitude)
      })
    }
  }

  render() {
    const { point, categories, mainLocation } = this.props
    if (point) {
      let {
        id,
        photos,
        name,
        rate,
        description,
        description_2: modeOperation,
        cost_sum: costValue,
        phone,
        latitude,
        longitude,
        site,
      } = point
      return (
        <div className="point-detail-wrapper">
          <div className="point-detail">
            <PointPhoto photos={photos} pointId={id} />
            <div className="point-info">
              <PointHeader categories={categories}>
                <PointRating rate={rate} />
              </PointHeader>
              <PointName name={name} size={24} />
              <PointParagraph description={description} />
              <PointParagraph description={modeOperation} caption="Режим работы" />
              <PointParagraph description={costValue} caption="Стоимость" />
            </div>
          </div>
          <div className="info-line">
            <img className="info-line__icon" src={Phone} alt="phone" />
            {phone !== '' ? (
              <a className="info-line__value" href={`tel:${phone}`}>
                {phone}
              </a>
            ) : (
              '-'
            )}
          </div>
          <div className="info-line">
            <img className="info-line__icon" src={Link} alt="link" />
            {site !== '' ? (
              <a className="info-line__value info-line__value--link" target="_blank" href={site}>
                {site}
              </a>
            ) : (
              '-'
            )}
          </div>
          <InformationAddress addressId={id} />
          <Map mainLocation={mainLocation} pointLocation={{ latitude: latitude, longitude: longitude }} />
        </div>
      )
    }
    return null
  }
}
