/* global google */
import React from 'react'
import { compose, withProps, lifecycle } from 'recompose'
import { withScriptjs, withGoogleMap, GoogleMap, Marker, DirectionsRenderer } from 'react-google-maps'

const MapWithADirectionsRenderer = compose(
  withProps({
    googleMapURL:
      'https://maps.googleapis.com/maps/api/js?key=AIzaSyC4R6AN7SmujjPUIGKdyao2Kqitzr1kiRg&v=3.exp&libraries=geometry,drawing,places',
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `148px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap,
  lifecycle({
    componentDidMount() {
      const DirectionsService = new google.maps.DirectionsService()
      const { mainLocation, pointLocation } = this.props
      if (!mainLocation) return
      DirectionsService.route(
        {
          origin: new google.maps.LatLng(mainLocation.latitude, mainLocation.longitude),
          destination: new google.maps.LatLng(pointLocation.latitude, pointLocation.longitude),
          travelMode: google.maps.TravelMode.DRIVING,
        },
        (result, status) => {
          if (status === google.maps.DirectionsStatus.OK) {
            this.setState({
              directions: result,
            })
          } else {
            console.error(`error fetching directions ${result}`)
          }
        },
      )
    },
  }),
)(props => {
  let { mainLocation, pointLocation } = props
  if (mainLocation) {
    return (
      <GoogleMap defaultZoom={15} defaultCenter={new google.maps.LatLng(mainLocation.latitude, mainLocation.longitude)}>
        {props.directions && <DirectionsRenderer directions={props.directions} />}
      </GoogleMap>
    )
  } else {
    return (
      <GoogleMap
        defaultZoom={15}
        defaultCenter={new google.maps.LatLng(pointLocation.latitude, pointLocation.longitude)}
      >
        {pointLocation && <Marker position={{ lat: pointLocation.latitude, lng: pointLocation.longitude }} />}
      </GoogleMap>
    )
  }
})

export default MapWithADirectionsRenderer
