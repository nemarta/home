import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import logoRed from 'assets/svgs/logoRed.svg'
import './HomeScreen.css'

export default class HomeScreen extends Component {
  render() {
    return (
      <div className="home-screen">
        <div className="home-content">
          <img className="home-content__logo" src={logoRed} alt="Logo" />
          <Link className="home-content__link" to="/points">
            Список точек
          </Link>
        </div>
      </div>
    )
  }
}
