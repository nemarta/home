import haversine from 'haversine'

export const cutTextByLimitCharacters = (text, limitCharacters) => {
  return text.substr(0, limitCharacters) + '...'
}

export const hasSelectedFilter = (filter, element) => {
  return filter !== null ? element.category_id.some(category => +category === +filter) : true
}

export const makeCancelablePromise = promise => {
  let isCancel = false

  let promiseWrapper = new Promise((resolve, reject) => {
    promise.then(
      data => (isCancel ? reject({ isCanceled: true }) : resolve(data)),
      error => (isCancel ? reject({ isCanceled: true }) : reject(error)),
    )
  })

  return {
    promise: promiseWrapper,
    cancel() {
      isCancel = true
    },
  }
}

export const convertToKilometers = meters => {
  let need = needToCalculate(meters)
  return {
    unit: need ? 'kilometer' : 'meters',
    value: need ? (meters / 1000).toFixed(1) : meters.toFixed(1),
  }
}

export const needToCalculate = value => {
  return value > 999
}

export const compareNumeric = (a, b) => {
  return a.distance > b.distance ? 1 : -1
}

export const sortDistances = (points, mainLocation) => {
  let arrayDistances = points.map(point => {
    return {
      id: point.id,
      distance: haversine(mainLocation, { latitude: point.latitude, longitude: point.longitude }, { unit: 'meters' }),
    }
  })
  arrayDistances.sort(compareNumeric)
  return arrayDistances
}

export const pageToTop = () => {
  console.log('tope')
  window.scrollTo(0, 0)
}
