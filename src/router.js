import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import HomeScreen from 'components/HomeScreen'
import PointsListScreen from 'components/PointsListScreen'
import PointScreen from 'components/PointScreen'

export default () => (
  <Router>
    <Switch>
      <Route exact path="/" component={HomeScreen} />
      <Route path="/points" component={PointsListScreen} />
      <Route path="/point/:id" component={PointScreen} />
    </Switch>
  </Router>
)
