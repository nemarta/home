import * as types from './types'
import { getIsNeedUpdate, getIsFetching, getSelectedPoint } from './selectors'
import * as api from 'api'

export const contentStart = () => ({
  type: types.CONTENT_START,
})

export const contentSuccess = payload => ({
  type: types.CONTENT_SUCCESS,
  payload,
  lastUpdate: Number(new Date()),
})

export const contentError = payload => ({
  type: types.CONTENT_ERROR,
  payload,
})

export const contentThunk = id => (dispatch, getState) => {
  if (getIsFetching(getState())) return
  if (getIsNeedUpdate(getState())) {
    dispatch(contentStart())
    return api
      .getContent()
      .then(response => dispatch(contentSuccess(response.data)))
      .then(response => (id ? getSelectedPoint(getState(), id) : null))
      .catch(error => dispatch(contentError(error)))
  }
}
