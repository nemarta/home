import * as types from './types'

const initialState = {
  isFetching: false,
  error: null,
  points: null,
  categories: null,
  lastUpdate: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.CONTENT_START:
      return {
        ...state,
        isFetching: true,
        error: null,
      }
    case types.CONTENT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        error: null,
        points: action.payload.points,
        categories: action.payload.categories,
        lastUpdate: action.lastUpdate,
      }
    case types.CONTENT_ERROR:
      return {
        ...state,
        isFetching: false,
        error: action.payload,
        points: null,
        lastUpdate: null,
      }
    default:
      return state
  }
}
