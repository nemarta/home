import { createSelector } from 'reselect'
import haversine from 'haversine'
import { getMainLocation } from 'reducers/location/selectors'
import { getPointsFavorite } from 'reducers/favorite/selectors'
import { onlyFavorite, getFilteredCategory, getKeywords } from 'reducers/filter/selectors'
import { convertToKilometers, sortDistances } from 'helpers'

export const getIsFetching = state => state.content.isFetching
export const getError = state => state.content.error

export const getPoints = state => (state.content.points ? state.content.points : null)
export const getPointsToObject = createSelector(getPoints, points => {
  if (!points) return null
  let result = {}
  points.forEach(point => (result[point.id] = point))
  return result
})
export const getSelectedPoint = (state, pointId) => {
  let points = getPointsToObject(state)
  return points ? points[pointId] : null
}
export const getPointLocation = (state, pointId) => {
  let point = getPointsToObject(state)
  return {
    latitude: point[pointId].latitude,
    longitude: point[pointId].longitude,
  }
}

export const getCategories = state => (state.content.categories ? state.content.categories : null)
export const getCategoriesToObject = createSelector(getCategories, categories => {
  let result = {}
  categories.forEach(category => (result[category.id] = category))
  return result
})
export const getFullCategoriesByIds = (state, categoriesIds) => {
  let categories = getCategoriesToObject(state)
  if (!categories) return null
  let result = []
  categoriesIds.forEach(id => result.push(categories[id]))
  return result
}

export const getLastUpdate = state => state.content.lastUpdate
export const getIsNeedUpdate = state =>
  getLastUpdate(state) === null ||
  getPoints(state) === null ||
  getError(state) !== null ||
  Number(new Date()) - getLastUpdate(state) > 1000 * 60

export const getDistance = (state, id) => {
  if (!getMainLocation(state)) return null
  let result = {
    meters: haversine(getMainLocation(state), getPointLocation(state, id), { unit: 'meter' }),
  }
  result['convertedData'] = convertToKilometers(result.meters)
  return result
}

export const getFilteredPoints = createSelector(
  getPoints,
  getPointsToObject,
  getFilteredCategory,
  onlyFavorite,
  getPointsFavorite,
  (points, pointsObject, filteredCategory, isOnlyFavorite, favoritePointsIds) => {
    if (!points) return null
    if (isOnlyFavorite) return favoritePointsIds ? favoritePointsIds.map(pointsId => pointsObject[pointsId]) : null
    if (filteredCategory) {
      let result = []
      points.forEach(point => (point.category_id.includes(filteredCategory) ? result.push(point) : null))
      return result
    }
    return points
  },
)

export const getFoundedPoints = createSelector(getFilteredPoints, getKeywords, (points, keywords) => {
  if (keywords.length < 3) return points
  return points.filter(
    point =>
      point.name.toLowerCase().indexOf(keywords.toLowerCase()) !== -1 ||
      point.description.toLowerCase().indexOf(keywords.toLowerCase()) !== -1,
  )
})

export const getPointsSortedAndFiltered = createSelector(
  getPointsToObject,
  getFoundedPoints,
  getMainLocation,
  (points, foundedPoints, mainLocation) => {
    if (!foundedPoints) return null
    if (!mainLocation) return foundedPoints
    let pointsCopy = foundedPoints.map(point => point)
    let sortData = sortDistances(pointsCopy, mainLocation)
    return sortData.map(element => points[element.id])
  },
)
