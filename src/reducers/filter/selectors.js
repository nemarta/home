export const getFilteredCategory = state => (state.filter.selectedCategory ? state.filter.selectedCategory : null)
export const onlyFavorite = state => state.filter.onlyFavorite
export const getKeywords = state => state.filter.keywords
