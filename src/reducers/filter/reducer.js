import * as types from './types'

const initialState = {
  selectedCategory: null,
  onlyFavorite: false,
  keywords: '',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.FILTER_CATEGORY_SELECT:
      return {
        ...state,
        selectedCategory: action.payload,
      }
    case types.FILTER_CLEAR_SELECTED_CATEGORIES:
      return {
        ...state,
        selectedCategory: null,
      }
    case types.FILTER_ONLY_FAVORITE:
      return {
        ...state,
        onlyFavorite: !state.onlyFavorite,
      }
    case types.FILTER_FAVORITE_CLEAR:
      return {
        ...state,
        onlyFavorite: false,
      }
    case types.FILTER_SET_KEYWORDS:
      return {
        ...state,
        keywords: action.payload,
      }
    case types.FILTER_CLEAR_KEYWORDS:
      return {
        ...state,
        keywords: '',
      }
    default:
      return state
  }
}
