import * as types from './types'

export const selectCategory = id => ({
  type: types.FILTER_CATEGORY_SELECT,
  payload: id,
})

export const clearSelectedCategory = () => ({
  type: types.FILTER_CLEAR_SELECTED_CATEGORIES,
})

export const toggleOnlyFavorite = () => ({
  type: types.FILTER_ONLY_FAVORITE,
})

export const onlyFavoriteClear = () => ({
  type: types.FILTER_FAVORITE_CLEAR,
})

export const setKeywords = payload => ({
  type: types.FILTER_SET_KEYWORDS,
  payload,
})

export const clearKeywords = () => ({
  type: types.FILTER_CLEAR_KEYWORDS,
})
