import { combineReducers } from 'redux'

import content from './content/reducer'
import address from './address/reducer'
import filter from './filter/reducer'
import location from './location/reducer'
import favorite from './favorite/reducer'

export default combineReducers({
  content,
  address,
  filter,
  location,
  favorite,
})
