import * as types from './types'

const initialState = {
  isFetching: false,
  mainLocation: null,
  error: null,
  lastUpdate: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.LOCATION_START:
      return {
        ...state,
        isFetching: true,
        error: null,
      }
    case types.LOCATION_SUCCESS:
      return {
        ...state,
        isFetching: false,
        mainLocation: action.payload.coords,
        error: null,
        lastUpdate: action.payload.lastUpdate,
      }
    case types.LOCATION_ERROR:
      return {
        ...state,
        isFetching: false,
        error: action.payload,
        lastUpdate: null,
      }
    default:
      return state
  }
}
