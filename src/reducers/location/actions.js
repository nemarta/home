import * as types from './types'
import { isFetching, getIsNeedUpdate, getError } from './selectors'

export const locationStart = () => ({
  type: types.LOCATION_START,
})

export const locationSuccess = payload => ({
  type: types.LOCATION_SUCCESS,
  payload,
})

export const locationError = payload => ({
  type: types.LOCATION_ERROR,
  payload,
})

export const locationThunk = () => (dispatch, getState) => {
  if (isFetching(getState())) return
  if (!getError(getState()) && !getIsNeedUpdate(getState())) return
  if (getIsNeedUpdate(getState())) {
    dispatch(locationStart())
    let locationPromise = new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        position => {
          resolve(position)
        },
        error => {
          reject(error)
        },
        {
          timeout: 10000,
        },
      )
    })
    locationPromise
      .then(position => {
        let locationData = {
          coords: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          },
          lastUpdate: position.timestamp,
        }
        dispatch(locationSuccess(locationData))
      })
      .catch(error => {
        dispatch(locationError(error.message))
      })
  }
}
