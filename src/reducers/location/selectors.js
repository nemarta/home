export const getMainLocation = state => (state.location.mainLocation ? state.location.mainLocation : null)
export const isFetching = state => (state.location.isFetching ? state.location.isFetching : null)
export const getError = state => (state.location.error ? state.location.error : null)
export const getLastUpdate = state => state.location.lastUpdate
export const getIsNeedUpdate = state =>
  getLastUpdate(state) === null ||
  getMainLocation(state) === null ||
  getError(state) !== null ||
  Number(new Date()) - getLastUpdate(state) > 1000 * 60
