import * as types from './types'

export const favoriteAdd = pointId => ({
  type: types.FAVORITE_ADD,
  pointId,
})

export const favoriteRemove = pointId => ({
  type: types.FAVORITE_REMOVE,
  pointId,
})
