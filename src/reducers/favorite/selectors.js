export const getPointsFavorite = state => (state.favorite.length > 0 ? state.favorite : null)
export const isFavorite = (state, pointId) => {
  let favoritePoints = getPointsFavorite(state)
  if (!favoritePoints) return null
  return favoritePoints.includes(pointId)
}
