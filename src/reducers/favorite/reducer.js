import * as types from './types'

const removeElement = (state, id) => {
  let result = []
  state.forEach(element => (element !== id ? result.push(element) : false))
  return result
}

export default (state = [], action) => {
  switch (action.type) {
    case types.FAVORITE_ADD:
      return [...state, action.pointId]
    case types.FAVORITE_REMOVE: {
      return [...removeElement(state, action.pointId)]
    }
    default:
      return state
  }
}
