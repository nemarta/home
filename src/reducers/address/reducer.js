import * as types from './types'

const initialState = {
  isFetching: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.ADDRESS_START:
      return {
        ...state,
        isFetching: true,
        [action.id]: {
          payload: null,
          error: null,
        },
      }
    case types.ADDRESS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        [action.id]: {
          payload: action.payload,
          error: null,
        },
      }
    case types.ADDRESS_ERROR:
      return {
        ...state,
        isFetching: false,
        [action.id]: {
          payload: null,
          error: action.payload,
        },
      }
    default:
      return state
  }
}
