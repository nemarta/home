export const getPointAddress = (state, id) => (state.address[id] ? state.address[id] : null)
export const getIsFetching = state => state.address.isFetching
