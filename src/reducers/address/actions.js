import * as types from './types'
import * as api from 'api'
import { getPointAddress } from './selectors'

export const addressStart = id => ({
  type: types.ADDRESS_START,
  id,
})

export const addressSuccess = (id, payload) => ({
  type: types.ADDRESS_SUCCESS,
  id,
  payload,
})

export const addressError = (id, payload) => ({
  type: types.ADDRESS_ERROR,
  payload,
})

export const addressThunk = (id, latitude, longitude) => (dispatch, getState) => {
  if (getPointAddress(getState(), id)) return
  dispatch(addressStart(id))
  api
    .getAddress(latitude, longitude)
    .then(response => {
      let result = response.data.results[0].formatted_address
      dispatch(addressSuccess(id, result))
    })
    .catch(error => {
      dispatch(addressError(error))
    })
}
