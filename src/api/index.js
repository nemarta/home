import { API_TOKEN, API_URL, API_VERSION } from 'config'
import axios from 'axios'

export const getContent = () => {
  return axios.get(`${API_URL}/${API_VERSION}/content`, {
    headers: {
      Authorization: `Token ${API_TOKEN}`,
    },
  })
}

export const getAddress = (latitude, longitude) =>
  axios.get(`http://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}`)
